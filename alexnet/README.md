Download alexnet: [https://drive.google.com/open?id=1FPa-jCDwXM4wi6ITFUOwgSnliv9_JL1T](https://drive.google.com/open?id=1FPa-jCDwXM4wi6ITFUOwgSnliv9_JL1T)  
  
If you have python in your system, you can directly download using following command. 

```ruby
python download_alexnet_caffemodel.py
```